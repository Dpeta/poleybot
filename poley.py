#!/usr/bin/env python3
"""yippee"""
import discord

from emoteroles import EMOTEROLES, SUPPORT_ROLES
from bottoken import TOKEN

intents = discord.Intents(
    members=True,
    guilds=True,
    guild_reactions=True,
    guild_messages=False,
    message_content=False,
    emojis_and_stickers=False,
)


class Poley(discord.Client):
    """epic wemons bot for role assigning"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.msgids = list(EMOTEROLES.keys()) + list(
            SUPPORT_ROLES.keys()
        )

    async def on_ready(self):
        """Call stuff for after start also."""
        print(f"We have logged in as {client.user}")

    async def on_raw_reaction_add(self, event):
        """
        This has to be RAW since we're checking old messages.

        It's possible this gets called before guild is set but
        I don't rly want to check for that on every call!!
        """
        # print("React added!")
        if not event.message_id in self.msgids:
            return


        # Check if is listed PCR msg
        if event.message_id in EMOTEROLES:
            guild = client.get_guild(761299023121350726)  # PCR
            rolesrc = EMOTEROLES
        # Check if is listed support server msg
        elif event.message_id in SUPPORT_ROLES:
            guild = client.get_guild(906250212362842143)  # Support server
            rolesrc = SUPPORT_ROLES
        else:
            return  # No matches

        #print(f"{str(event.emoji).encode('unicode_escape')}")

        # Check if emoji matches
        if event.emoji.id and event.emoji.id in rolesrc[event.message_id].keys():  # Custom emote id comparison
            role = guild.get_role(rolesrc[event.message_id][event.emoji.id])
        elif str(event.emoji) in rolesrc[event.message_id].keys():  # Unicode emote str comparison
            role = guild.get_role(rolesrc[event.message_id][str(event.emoji)])
        else:
            return

        await event.member.add_roles(role)

    async def on_raw_reaction_remove(self, event):
        """Remove evil roles..."""
        # print("React removed!")
        if not event.message_id in self.msgids:
            return

        # Check if is listed PCR msg
        if event.message_id in EMOTEROLES:
            guild = client.get_guild(761299023121350726)  # PCR
            rolesrc = EMOTEROLES
        # Check if is listed support server msg
        elif event.message_id in SUPPORT_ROLES:
            guild = client.get_guild(906250212362842143)  # Support server
            rolesrc = SUPPORT_ROLES
        else:
            return  # No matches
        # Check if emoji matches
        if event.emoji.id and event.emoji.id in rolesrc[event.message_id].keys():  # Custom emote id comparison
            role = guild.get_role(rolesrc[event.message_id][event.emoji.id])
        elif str(event.emoji) in rolesrc[event.message_id].keys():  # Unicode emote str comparison
            role = guild.get_role(rolesrc[event.message_id][str(event.emoji)])
        else:
            return
        member = guild.get_member(event.user_id)  # Remove events members lol
        if not member:
            print("failed to get member")
            return
        await member.remove_roles(role)


client = Poley(intents=intents, max_messages=None)
client.run(TOKEN)
